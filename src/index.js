import {jsonFields, fieldSpy, fieldTypes, reduceJSON} from './jsqlon/json.js';
import {extendedType} from './jsqlon/types.js';


let jsqlon = {jsonFields, fieldSpy, fieldTypes, reduceJSON, extendedType}

export default jsqlon
