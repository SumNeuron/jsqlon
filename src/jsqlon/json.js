import {extendedType} from './types.js'

/**
 * Gives all fields for all records in SQL-like object
 * @param {object} json - an object of {id: record} pairs
 */
export function jsonFields(json) {
  let all = [].concat(
    ...Object.keys(json)
    .map((key) => Object.keys(json[key]))
  );
  let unique = [...new Set(all)].sort();
  return unique;
}

/**
 * Given an SQL-like object and a field, returns the type of value for that field
 * @param {object} json - an object of {id: record} pairs
 * @param field - a field which could be found in record, e.g. record[field]
 */
export function fieldSpy(json, field) {
  let first = Object.keys(json)[0];
  let value = json[first][field];
  return extendedType(value);
}

/**
 * Given an SQL-like object and a list of fields, returns the types for the values of those fields
 * @param {object} json - an object of {id: record} pairs
 * @param {array} fields - an array of fields which can be found in any given record, e.g. record[fields[i]]
 */
export function fieldTypes(json, fields) {
  let types = {};
  fields.forEach(field => {
    types[field] = { type: fieldSpy(json, field) };
  });
  return types;
}


export function reduceJSON(json, keys) {
  return Object.keys(json)
    .filter(key => keys.includes(key))
    .reduce((obj, key) => {
      obj[key] = json[key];
      return obj;
    }, {});
}
