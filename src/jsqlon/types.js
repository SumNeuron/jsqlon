/**
 * A slight extension of the vanilla JS types
 * @param variable - any js variable
 */
export function extendedType(variable) {
  // start with JS basics
  let type = typeof variable;

  /*
  number and string are sufficient to know which functions and conditionals
  can be applied to them.
  */
  if (["number", "string"].includes(type)) {
    return type;
  }

  /*
  if not a number or string, it might be an array.
  What the elements are of that array might modify what functions
  can be applied. E.g. an array of numbers allows for min, max, mean,
  etc to be applied. Whereas an array of strings does not.
  */
  if (type != "object") {
    return type;
  }

  /* only dealing with object types at this point */
  if (Array.isArray(variable)) {
    type = "array";
    if (!variable.length) {
      return type;
    }

    let subtypes = variable.map((e) => typeof e);
    let subtype = subtypes[0];
    let allSameQ = subtypes.every(e => e === subtype);

    if (allSameQ) {
      type = `${type}:${subtype};`
    }
    return type;
  }
  return type;
}
